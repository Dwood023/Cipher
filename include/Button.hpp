#ifndef BUTTON_HPP
#define BUTTON_HPP

#include <SFML/Graphics.hpp>
#include "Menu.hpp"
#include "Player.hpp"

class Button {
	
	public:

		Button(void (Player::*const action)(), const Camera& camera, const sf::Font& font);
		void (Player::*const press)(/*Whatever all actions need*/);
		bool isPressed(sf::RenderWindow& window, const Camera& camera);
		void draw(sf::RenderWindow& window);

	private:

		Menu button;

};

#endif /* BUTTON_HPP */
