import re, sys

dataFile = open("./map.data")
positionFile = open("./WorldMapPositions.txt")
    
dataList = dataFile.readlines

output = []

for line in dataFile:
    output.append(line)
    if re.search("\[.+\]", line): # If country tag
        posLine = positionFile.readline()
        posLine = ", ".join(posLine.split(",")[1:])
        posLine = "  WorldPosition : " + posLine
        print(posLine)
        output.append(posLine)

print("".join(output))
