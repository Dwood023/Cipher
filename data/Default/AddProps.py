import re, sys

dataFile = open("./map.data")
outputFile = open("./outputFile")

output =[]

for line in dataFile:
    if re.search("\[.+\]", line):
        milPow = input("Enter MP modifier for " + line)
        forceP = input("Enter FP modifier for " + line)
        line = line + "  MilitaryPower : " + milPow + "\n"
        line = line + "  ForceProjection : " + forceP + "\n"

    print(line, end="")
    output.append(line)

outputFile.write("".join(output))
print("".join(output))
