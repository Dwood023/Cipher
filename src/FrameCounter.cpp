#include <SFML/Graphics.hpp>
#include <cmath>
#include "FrameCounter.hpp"
#include "Camera.hpp"

FrameCounter::FrameCounter(const sf::Font& font) {
	fps = sf::Text("N/A", font, 20);
}
void FrameCounter::setCurrentFPS(float frameRate) {
	fps.setString(std::to_string(frameRate) + "Hz");
}

void FrameCounter::setPosition(float x, float y) {
	fps.setPosition(x, y);
}

void FrameCounter::setPosition(Camera camera) {
	sf::Vector2f pos = camera.getView().getCenter();
	sf::Vector2f size = camera.getView().getSize();

	setPosition(pos.x - size.x / 2, pos.y + (size.y / 2) - fps.getCharacterSize());

}

sf::Text FrameCounter::getText() { return fps; }
