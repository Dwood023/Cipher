#include <SFML/Graphics.hpp>
#include <iostream>
#include "Country.hpp"
#include "Camera.hpp"
#include "Menu.hpp"
#include "Assets.hpp"
#include "Player.hpp"
#include "Button.hpp"

int main() {
	sf::RenderWindow window(sf::VideoMode(2560, 1440), "");

	Assets::load();

	sf::Time deltaTime = sf::seconds(1.0f/60.0f);
	sf::Clock clock;

	sf::Font font;
	font.loadFromFile("./data/Shared/AnonymousPro-Regular.ttf");

	Camera mapCam(window);
	Camera hudCam(window);

	Player AI = Player(sf::Color::Yellow);
	Player player1(sf::Color::Cyan);

	AI.addAll();
	player1.getStarter();

	Player& localPlayer = player1;

	mapCam.centerAt(sf::Vector2f(localPlayer.getSelectedCountry()->getBounds().left, localPlayer.getSelectedCountry()->getBounds().top));

	Menu menu(sf::Vector2f(700, 200), Anchor::TOP, hudCam, font);
	Button button(&Player::attack, hudCam, font);

	while (window.isOpen()) {
		sf::Event event;

		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::Resized) {
				mapCam.resize(window);
				hudCam.resize(window);
				menu.setPosition(hudCam);
			}
			// Camera Zoom
			if (event.type == sf::Event::MouseWheelMoved)
				mapCam.zoom(event.mouseWheel.delta);
			if (event.type == sf::Event::MouseButtonPressed)
				if (button.isPressed(window, hudCam))
					(localPlayer.*button.press)();
		}

		// Camera Movement
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			mapCam.move(Direction::UP, deltaTime);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			mapCam.move(Direction::LEFT, deltaTime);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			mapCam.move(Direction::DOWN, deltaTime);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			mapCam.move(Direction::RIGHT, deltaTime);

		window.clear(sf::Color::Blue);

		// Update
		localPlayer.update(window);
		if (localPlayer.getHoveredCountry())
			menu.setContent(player1.getHoveredCountry()->getName() + 
					"\nMilitary Power : " + std::to_string(localPlayer.getHoveredCountry()->getMilitaryPower()) + 
					"\nForce Projection : " + std::to_string(localPlayer.getHoveredCountry()->getForceProjection()));

		// Map elements
		window.setView(mapCam.getView());
		// Draw map from perspective of localPlayer
		AI.draw(window);
		localPlayer.draw(window);

		// Hud elements
		window.setView(hudCam.getView());
		menu.draw(window);
		button.draw(window);

		// For calculations in world space next loop
		window.setView(mapCam.getView());

		window.display();

		deltaTime = clock.restart();

	}
	return 0;
}
