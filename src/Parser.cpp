#include <SFML/Graphics.hpp>
#include <regex>
#include <vector>
#include <fstream>
#include <cassert>
#include "Country.hpp"
#include "Parser.hpp"
#include "Assets.hpp"

namespace Parser {

	std::vector<Country> parseCountries() {

		std::vector<Country> output;

		std::ifstream dataFile;
		dataFile.open("./data/Default/map.data");

		const std::regex rgxName("(\\[)(.+)(\\])");
		const std::regex rgxPos("\\s*WorldPosition\\s?:\\s?([0-9]+)\\s?,\\s?([0-9]+)\\s*");
		const std::regex rgxRect("\\s*TextureRect\\s?:\\s?([0-9]+)\\s?,\\s?([0-9]+)\\s?,\\s?([0-9]+)\\s?,\\s?([0-9]+)\\s*");

		std::string line;
		std::smatch match;

		// Props
		std::string name; bool nameSet;
		sf::Vector2i pos; bool posSet;
		sf::IntRect rect; bool rectSet;

		while (std::getline(dataFile, line)) { // For each line

			// [COUNTRY-TAG]
			if (std::regex_match(line, match, rgxName)) {
				// Discard previous country data and start a new one
				name = match[2];
				nameSet = true;
				posSet = false;
				rectSet = false;
			}
			// WorldPosition : XXXX, XXXX
			if (std::regex_match(line, match, rgxPos)) {
				pos = sf::Vector2i(std::stoi(match[1]), 
									std::stoi(match[2]));
				posSet = true;
			}
			// TextureRect : XXXX, XXXX, XXXX, XXXX
			if (std::regex_match(line, match, rgxRect)) {
				rect = sf::IntRect(std::stoi(match[1]), 
									std::stoi(match[2]), 
									std::stoi(match[3]),
									std::stoi(match[4]));
				rectSet = true;
			}

			// If a complete set of properties are found...
			if (nameSet && posSet && rectSet)
				output.push_back(Country(name, pos, Assets::getCountryTexture(), rect));
		}

		dataFile.close();
		
		// If we haven't picked up any countries by now, everything isn't going to work from here
		assert(output.size() > 0); 

		return output;
	}
}
