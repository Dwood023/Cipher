#include "MathLib.hpp"
#include <stdlib.h>
#include <random>

namespace MathLib {
	
	int rand(int min, int max) {
		
		std::random_device randDevice;
		std::mt19937 twister(randDevice());

		std::uniform_real_distribution<> range(min, max);

		return range(twister);
	}

	bool getProc(int percentChance) {
		return (MathLib::rand(0, 100) < percentChance);
	}
}
