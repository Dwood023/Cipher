#include <SFML/Graphics.hpp>
#include <iostream>
#include "Menu.hpp"
#include "Camera.hpp"

Menu::Menu(sf::Vector2f size, Anchor position, const Camera& camera, const sf::Font& font) 
:
	box(size),
	text(sf::Text("N/A", font, 30)),
	anchor(position)
{

	setPosition(camera);
	text.setFillColor(sf::Color::Black);
	box.setOutlineColor(sf::Color::Black);
	box.setOutlineThickness(3);

}
void Menu::setPosition(const Camera& camera) {

	const sf::Vector2f centered(camera.getCenter().x - (box.getSize().x / 2),
								camera.getCenter().y - (box.getSize().y / 2));

	const float left = camera.getDimensions().left;
	const float top = camera.getDimensions().top;
	const float right = left + camera.getDimensions().width;
	const float bottom = top + camera.getDimensions().height;

	if (anchor == Anchor::TOP)
		box.setPosition(centered.x, top);
	if (anchor == Anchor::LEFT)
		box.setPosition(left, centered.y);
	if (anchor == Anchor::BOTTOM)
		box.setPosition(centered.x, bottom);
	if (anchor == Anchor::RIGHT)
		box.setPosition(right, centered.y);
	if (anchor == Anchor::CENTER)
		box.setPosition(centered.x, centered.y);

	alignText();

}

void Menu::setContent(std::string content) {

	text.setString(content);

	const float linewraps = (box.getSize().x - (padding.x * 2)) / text.getGlobalBounds().width;
	const int interval = content.size() * linewraps;

	for (int strPos = interval - 1; strPos < content.size(); strPos += interval)
		content.insert(strPos, "\n");

	text.setString(content);
	alignText();

}

void Menu::alignText() {
	
	text.setPosition(box.getGlobalBounds().left + padding.x, box.getGlobalBounds().top + padding.y);
}

void Menu::draw(sf::RenderWindow& window) {
	window.draw(box);
	window.draw(text);
}

sf::FloatRect Menu::getDimensions() {
	return box.getGlobalBounds();
}
