#ifndef MENU_HPP
#define MENU_HPP

#include <SFML/Graphics.hpp>
#include "Camera.hpp"

enum class Anchor {
	TOP, LEFT, BOTTOM, RIGHT, CENTER
};

class Menu {

	public:

		Menu(sf::Vector2f size, Anchor position, const Camera& camera, const sf::Font& font);
		void setContent(std::string content);
		void setPosition(const Camera& camera);
		void draw(sf::RenderWindow& window);
		sf::FloatRect getDimensions();

	private:

		const sf::Vector2f padding = sf::Vector2f(20, 10);
		void alignText();
		sf::RectangleShape box;
		sf::Text text;
		Anchor anchor;

};

#endif /* MENU_HPP */
