#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <SFML/Graphics.hpp>
#include <vector>
#include "Country.hpp"

class Player {

	public:
		Player(sf::Color c);
		void update(sf::RenderWindow& window);
		void draw(sf::RenderWindow& window);
		void addAll();
		void remove(Country& country);
		void add(Country& country);
		void take(Country& country);
		void getStarter();

		//Actions
		void attack();

		const Country* const getSelectedCountry();
		const Country* const getHoveredCountry();
		const Country* const getTargetCountry();

	private:

		std::vector<Country*> countries;

		sf::Color playerColor;

		Country* selectedCountry = nullptr;
		Country* hoveredCountry = nullptr;
		Country* targetCountry = nullptr;

		static std::vector<Country> gameCountries;
};

#endif /* PLAYER_HPP */
