#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <SFML/Graphics.hpp>

enum class Direction {
	UP, LEFT, DOWN, RIGHT
};

class Camera {

	public:

		Camera(sf::RenderWindow& window);
		void move(Direction dir, sf::Time deltaTime);
		void centerAt(sf::Vector2f position);
		void resize(sf::RenderWindow& window);
		void zoom(int factor);
		sf::IntRect getDimensions() const;
		sf::Vector2f getCenter() const;
		sf::View getView() const;


	private:

		sf::View view;

};


#endif /* CAMERA_HPP */
