#ifndef COUNTRY_HPP
#define COUNTRY_HPP

#include <SFML/Graphics.hpp>
#include <string>

class Player;

class Country {

	public:
		Country(std::string name, sf::Vector2i worldPosition, const sf::Texture& texture, sf::IntRect textureRect);

		bool containsPoint(sf::Vector2f point) const;
		bool canReach(Country& enemy) const;
		void draw(sf::RenderWindow& window, sf::Color color);

		void setMilitaryPower(int newPower);

		void setOwner(Player& player);
		Player& getOwner() const;

		std::string getName() const;
		sf::Sprite getSprite() const;
		int getMilitaryPower() const;
		int getForceProjection() const;
		sf::FloatRect getBounds() const;

	private:
		const std::string name;
		int militaryPower;
		int forceProjection;
		sf::Sprite sprite;
		Player* owner = nullptr;


};

#endif /* COUNTRY_HPP */
