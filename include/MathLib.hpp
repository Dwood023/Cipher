#ifndef MATHLIB_HPP
#define MATHLIB_HPP

namespace MathLib {

	int rand(int min, int max);
	bool getProc(int percentChance);

}

#endif /* MATHLIB_HPP */
