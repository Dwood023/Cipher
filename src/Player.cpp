#include <SFML/Graphics.hpp>
#include <iostream>
#include "Player.hpp"
#include "Parser.hpp"
#include "Country.hpp"
#include "MathLib.hpp"

std::vector<Country> Player::gameCountries = Parser::parseCountries();

Player::Player(sf::Color color) {
	playerColor = color;
}

void Player::attack() {

	if (targetCountry && selectedCountry && selectedCountry->canReach(*targetCountry)) {
		// Success
		if (selectedCountry->getMilitaryPower() > targetCountry->getMilitaryPower()) {

			std::cout << "Captured " + targetCountry->getName() << std::endl;
			//targetCountry->setMilitaryPower(targetCountry->getMilitaryPower() * 3 / 4);
			//selectedCountry->setMilitaryPower(selectedCountry->getMilitaryPower() * 3 / 4);

			take(*targetCountry);
			targetCountry = nullptr;
		}
		//Failure
		else {
			std::cout << "Failed to capture " + targetCountry->getName() << std::endl;
			//selectedCountry->setMilitaryPower(selectedCountry->getMilitaryPower() / 2);
		}
	}

	else std::cout << "Can't reach or no target" << std::endl;

}

void Player::update(sf::RenderWindow& window) { 

	bool leftClick = sf::Mouse::isButtonPressed(sf::Mouse::Button::Left);
	bool nothingSelected = true;
	sf::Vector2f mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));

	hoveredCountry = nullptr;
	

	for (Country& country : gameCountries) {
		if (country.containsPoint(mousePos)) {
			if (leftClick) {
				if (&country.getOwner() == this)
					selectedCountry = &country;
				else targetCountry = &country;
				nothingSelected = false;
			}
			else hoveredCountry = &country;
		}
	}
	if (leftClick && nothingSelected) {
		selectedCountry = nullptr;
		targetCountry = nullptr;
	}

}
void Player::addAll() {
	for (Country& country : gameCountries) {
		countries.push_back(&country);
		country.setOwner(*this);
	}
}

void Player::getStarter() { 
	Country& starter = gameCountries[MathLib::rand(0, gameCountries.size() - 1)];
	take(starter);
	selectedCountry = &starter;
}

void Player::draw(sf::RenderWindow& window) {

	for (Country* country : countries)
		if (country != hoveredCountry &&
			country != selectedCountry &&
			country != targetCountry)
			country->draw(window, playerColor);

	if (hoveredCountry != nullptr)
		hoveredCountry->draw(window, sf::Color::Magenta);
	if (selectedCountry != nullptr)
		selectedCountry->draw(window, sf::Color::White);
	if (targetCountry != nullptr)
		targetCountry->draw(window, sf::Color::Red);

}

void Player::remove(Country& country) {
	countries.erase(std::remove(countries.begin(), countries.end(), &country));
}

void Player::add(Country& country) {
	countries.push_back(&country);
}

void Player::take(Country& country) {
	add(country);
	country.getOwner().remove(country);
	country.setOwner(*this);
}

const Country* const Player::getSelectedCountry() {
	return selectedCountry;
}
const Country* const Player::getHoveredCountry() {
	return hoveredCountry;
}
const Country* const Player::getTargetCountry() {
	return targetCountry;
}
