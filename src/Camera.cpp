#include <SFML/Graphics.hpp>
#include <cmath>
#include "Camera.hpp"

Camera::Camera(sf::RenderWindow& window) {
	resize(window);
}

void Camera::move(Direction dir, sf::Time deltaTime) {

	const float viewSpeed = 600;

	switch(dir)  {
		case Direction::UP : view.move(0,-viewSpeed * deltaTime.asSeconds()); break;
		case Direction::LEFT : view.move(-viewSpeed * deltaTime.asSeconds(), 0); break;
		case Direction::DOWN : view.move(0, viewSpeed * deltaTime.asSeconds()); break;
		case Direction::RIGHT : view.move(viewSpeed * deltaTime.asSeconds(), 0); break;
	}
}

void Camera::resize(sf::RenderWindow& window) {
	view.setSize(window.getSize().x, window.getSize().y);
}
void Camera::zoom(int factor) {

	const float scrollRate = 1.1;

	view.zoom(pow(scrollRate, factor * -1));
}

void Camera::centerAt(sf::Vector2f position) {
	view.setCenter(position);
}

sf::View Camera::getView() const { return view; }

sf::IntRect Camera::getDimensions() const {

	sf::Vector2f center = view.getCenter();
	sf::Vector2f size = view.getSize();

	int top = center.y - (size.y / 2);
	int left = center.x - (size.x / 2);

	return sf::IntRect(left, top, size.x, size.y);
}

sf::Vector2f Camera::getCenter() const {
	return view.getCenter();
}
