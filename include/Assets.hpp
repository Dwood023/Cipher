#ifndef ASSETS_HPP
#define ASSETS_HPP

#include <SFML/Graphics.hpp>

namespace Assets {

	//public:
	void load();

	const sf::Texture& getCountryTexture();
	const sf::Image& getCountryImage();

	// private:
	namespace {
		sf::Image countryImage;
		sf::Texture countryTexture;
	}
}

#endif /* ASSETS_HPP */
