#include <SFML/Graphics.hpp>
#include "Button.hpp"
#include "Player.hpp"

Button::Button(void (Player::*const action)(), const Camera& camera, const sf::Font& font) 
:
	press(action),
	button(Menu(sf::Vector2f(150, 100), Anchor::CENTER, camera, font))
{

}
bool Button::isPressed(sf::RenderWindow& window, const Camera& camera) {

	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
		sf::Vector2f mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window), camera.getView());
		return button.getDimensions().contains(mousePos);
	}
	else return false; 
}

void Button::draw(sf::RenderWindow& window) {
	button.draw(window);
}
