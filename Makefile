COMPILER=g++ -std=c++11 -Iinclude/
LIBS:=-lsfml-graphics -lsfml-window -lsfml-system
SRCFILES:=${wildcard src/*.cpp}
OBJFILES := $(addprefix obj/,$(notdir $(SRCFILES:.cpp=.o)))
EXECUTABLE:=Cipher

all: bin/$(EXECUTABLE)

bin/$(EXECUTABLE): $(OBJFILES)
	@echo "** Build Happening **"
	$(COMPILER) $(LIBS) -g -MMD -MP -o $@ $^ 

obj/%.o: src/%.cpp
	$(COMPILER) -g -c -MMD -MP -o $@ $<

clean: 
	@echo "** Removing executable and objects **"
	rm -f bin/$(EXECUTABLE) $(OBJFILES)

killswp:
	@echo "Cleaning up swp trash"
	rm src/.*.swp

debug:
	gdb -tui bin/${EXECUTABLE}

install:
	@echo "** Installing to /usr/bin **"
	cp bin/$(EXECUTABLE) /usr/bin
	
uninstall:
	@echo "** Uninstalling from /usr/bin **"
	rm -f /usr/bin/$(EXECUTABLE)

run:
	./bin/$(EXECUTABLE)
