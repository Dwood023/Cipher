#include <SFML/Graphics.hpp>
#include <string>
#include "Country.hpp"
#include "Assets.hpp"
#include "Player.hpp"
#include "Parser.hpp"

Country::Country(std::string name, sf::Vector2i worldPosition, const sf::Texture& texture, sf::IntRect textureRect) 
:
	name(name), 
	sprite(sf::Sprite(texture, textureRect))
{
	sprite.setPosition(worldPosition.x, worldPosition.y);
	forceProjection = 50 + (textureRect.width * textureRect.height / 15000);
	militaryPower = 50 + (textureRect.width * textureRect.height / 10000);

}

bool Country::containsPoint(sf::Vector2f point) const {

	sf::FloatRect mapRect = sprite.getGlobalBounds();

	if (mapRect.contains(point.x, point.y)) {

		int localX = point.x - mapRect.left;
		int localY = point.y - mapRect.top;

		int sheetX = localX + sprite.getTextureRect().left;
		int sheetY = localY + sprite.getTextureRect().top;

		return (Assets::getCountryImage().getPixel(sheetX, sheetY).a == 255);
	}

	else return false;
}

bool Country::canReach(Country& enemy) const {

	sf::FloatRect reachBox = sprite.getGlobalBounds();

	float reachProj = forceProjection / 2;

	reachBox.left -= reachProj;
	reachBox.top -= reachProj;
	reachBox.width += reachProj;
	reachBox.height += reachProj;

	return reachBox.intersects(enemy.getBounds());
}

sf::FloatRect Country::getBounds() const {
	return sprite.getGlobalBounds();
}

void Country::draw(sf::RenderWindow& window, sf::Color color) {

	sf::Sprite spriteCopy = sprite;
	spriteCopy.setColor(color);
	window.draw(spriteCopy);

}

void Country::setMilitaryPower(int newPower) {
	militaryPower = newPower;
}

void Country::setOwner(Player& player) {
	owner = &player;

}
Player& Country::getOwner() const {
	return *owner;

}
std::string Country::getName() const { return name; }
sf::Sprite Country::getSprite() const { return sprite; }
int Country::getMilitaryPower() const { return militaryPower; }
int Country::getForceProjection() const { return forceProjection; }
