#ifndef PARSER_HPP
#define PARSER_HPP

#include <SFML/Graphics.hpp>
#include "Country.hpp"
#include "Assets.hpp"

namespace Parser {

	std::vector<Country> parseCountries();

}

#endif /* PARSER_HPP */
