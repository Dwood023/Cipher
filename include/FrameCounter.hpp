#ifndef FRAMECOUNTER_HPP
#define FRAMECOUNTER_HPP

#include <SFML/Graphics.hpp>
#include "Camera.hpp"

class FrameCounter {
	public:
		FrameCounter(const sf::Font& font);
		void setCurrentFPS(float frameRate);
		void setPosition(float x, float y);
		void setPosition(Camera camera);
		sf::Text getText();
	private:
		sf::Text fps;
};


#endif /* FRAMECOUNTER_HPP */
