import re
import sys

rectFile = open("./TexturePositions.txt", 'r')

output = []

for line in rectFile:
    if not re.search("orig|index|offset|rotate", line): # Remove unused stuff

        ## Country Name #################
        if re.search("_[0-9]{4}_", line): 

            line = "[" + line.split("_")[2] # Delete _XXXX_ prefix and prepend [

            line = line.replace("-", " ") 

            index = line.find("\n")
            line = line[:index] + "]" + line[index:] # Insert ] before line feed

        ## TextureRect ##################
        elif re.search("xy", line):
            line = line.rstrip()
            line = line.replace("xy", "TextureRect")
        elif re.search("size", line):
            line = line.replace("  size:", ",")


        output.append(line )

print ("".join(output))
