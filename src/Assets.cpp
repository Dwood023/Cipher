#include <SFML/Graphics.hpp>
#include <iostream>
#include "Assets.hpp"

namespace Assets {

	void load() {

		if (!countryImage.loadFromFile("./data/Default/TextureSheet.png"))
			std::cout << "Image failed to load" << std::endl;
		if (!countryTexture.loadFromImage(countryImage))
			std::cout << "Texture failed to load" << std::endl;

	}

	const sf::Texture& getCountryTexture() {
		return countryTexture;
	}
	const sf::Image& getCountryImage() {
		return countryImage;
	}

}
